<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>List of clients</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/custom_style.css" rel="stylesheet">
    <script src="/resources/js/bootstrap.min.js"></script>

</head>
<body>
<div id="index_container" class="container-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-2">
        <a href="/addClient">
            <button type="button" class="btn btn-default">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add new client
            </button>
        </a>
    </div>
    <div class="col-md-3 input-group">
			<span class="input-group-addon" id="basic-addon1">
				<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
			</span>
        <input type="text" id="search" class="form-control" placeholder="Search">
    </div>
    <div class="col-md-4"></div>
</div>

<div class="row" style="margin-top: 30px">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <table class="table table-hover">
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Patronymic</th>
                <th>Email</th>
                <th>Date of birth</th>
                <th></th>
            </tr>
        <#list clients as client>
            <tr>
                <td>${client.name}</td>
                <td>${client.surname}</td>
                <td>${client.patronymic}</td>
                <td>${client.email}</td>
                <td>${client.dateOfBirth?string("yyyy-MM-dd")}</td>
                <td><a href="/clientPage?id=${client.id}"><strong>DETAILS</strong></a>
                </td>
            </tr>
        </#list>
        </table>
    </div>
    <div class="col-md-2"></div>
</div>

<script>
    $("#search").keyup(function () {
        var value = this.value.toLowerCase().trim();

        $("table tr").each(function (index) {
            if (!index) return;
            $(this).find("td").each(function () {
                var id = $(this).text().toLowerCase().trim();
                var not_found = (id.indexOf(value) == -1);
                $(this).closest('tr').toggle(!not_found);
                return not_found;
            });
        });
    });
</script>
</body>
</html>