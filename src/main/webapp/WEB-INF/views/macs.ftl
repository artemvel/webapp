<#macro cl_table cl>
<table class="table table-hover" style="margin-top: 20px">
    <tr>
        <th>STATUS</th>
        <#if cl.status == 'ACTIVE'>
            <th><strong style="color: green">ACTIVE</strong></th>
        <#else>
            <th><strong style="color: red">CLOSED</strong></th>
        </#if>
    </tr>
    <tr>
        <th>Open date</th>
        <th>${cl.openDate?string("yyyy-MM-dd")}</th>
    </tr>
    <tr>
        <th>Close date</th>
        <th>${cl.closeDate?string("yyyy-MM-dd")}</th>
    </tr>
    <tr>
        <th>Money</th>
        <th>${cl.limit}</th>
    </tr>
    <tr>
        <th>Currency</th>
        <th>${cl.currency}</th>
    </tr>
    <tr>
        <th>Rate</th>
        <th>${cl.rate}%</th>
    </tr>
    <tr>
        <th>Schema</th>
        <th>${cl.paymentSchema}</th>
    </tr>
</table>
</#macro>

<#macro py_table creditLine>
<table class="table table-hover">
    <tr>
        <th>Payment date</th>
        <th>Amount</th>
        <th></th>
    </tr>
    <#list creditLine.payments as payment>
    <#assign isPd = payment.paid>
        <#if isPd>
        <tr class="success">
        <#else>
        <tr>
        </#if>
            <td>${payment.payDay?string("yyyy-MM-dd")}</td>
            <td>${payment.amount} ${creditLine.currency}</td>
        </tr>
    </#list>
</table>
</#macro>

<#macro client_info_table client>
<table class="table table-condensed">
    <tr>
        <th>Name:</th>
    </tr>
    <tr>
        <td class="active">${client.name}</td>
    </tr>

    <tr>
        <th>Surname:</th>
    </tr>
    <tr>
        <td class="active">${client.surname}</td>
    </tr>

    <tr>
        <th>Patronymic:</th>
    </tr>
    <tr>
        <td class="active">${client.patronymic}</td>
    </tr>

    <tr>
        <th>Birth date:</th>
    </tr>
    <tr>
        <td class="active">${client.dateOfBirth?string("yyyy-MM-dd")}</td>
    </tr>

    <tr>
        <th>Home address:</th>
    </tr>
    <tr>
        <td class="active">${client.homeAddress}</td>
    </tr>

    <tr>
        <th>Email:</th>
    </tr>
    <tr>
        <td class="active">${client.email}</td>
    </tr>

    <tr>
        <th>TIN:</th>
    </tr>
    <tr>
        <td class="active">${client.TIN}</td>
    </tr>

    <tr>
        <th>Card No.:</th>
    </tr>
    <tr>
        <td class="active">${client.creditCardNumber}</td>
    </tr>

    <tr>
        <th>Phone:</th>
    </tr>
    <tr>
        <td class="active">${client.phone}</td>
    </tr>
</table>
</#macro>