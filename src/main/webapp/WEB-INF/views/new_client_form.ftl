<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/custom_style.css" rel="stylesheet">
    <script src="/resources/js/bootstrap.min.js"></script>
    <script src="/resources/js/validations.js"></script>
    <script src="/resources/js/date_picker.js"></script>

    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">

</head>
<body>
<div class="container" style="margin-top: 20px">
    <div class="col-md-2"></div>
    <div class="thumbnail col-md-7">
        <form id="client_form" class="form-horizontal" action="/createClient" method="post" enctype="multipart/form-data">

            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>
                <div class="col-sm-10">
                    <input id="nameInput" type="text" name="name" class="form-control" placeholder="Name">
                    <span id="nameError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Surname</label>
                <div class="col-sm-10">
                    <input id="surnameInput" type="text" name="surname" class="form-control" placeholder="Surname">
                    <span id="surnameError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Patronymic</label>
                <div class="col-sm-10">
                    <input id="patronymicInput" type="text" name="patronymic" class="form-control" placeholder="Patronymic">
                    <span id="patronymicError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Birth Date</label>
                <div class="col-sm-10">
                    <input id="dateOfBirthInput" type="text" name="dateOfBirth" class="form-control" placeholder="Birth Date">
                    <span id="dateOfBirthError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Home Address</label>
                <div class="col-sm-10">
                    <input id="homeAddressInput" type="text" name="homeAddress" class="form-control" placeholder="Home Address">
                    <span id="homeAddressError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input id="emailInput" type="text" name="email" class="form-control" placeholder="Email">
                    <span id="emailError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                    <input id="phoneInput" type="text" name="phone" class="form-control" placeholder="Your phone number">
                    <span id="phoneError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">TIN</label>
                <div class="col-sm-10">
                    <input id="tinInput" type="text" name="TIN" class="form-control" placeholder="Tax identification number">
                    <span id="tinError"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Card Number</label>
                <div class="col-sm-10">
                    <input id="creditCardNumberInput" type="text" name="creditCardNumber" class="form-control" placeholder="Credit Card Number">
                    <span id="creditCardNumberError"></span>
                </div>
            </div>
            <#--PHOTO-->
            <div class="form-group">
                <label class="col-sm-2 control-label">Photo</label>
                <div class="col-sm-10">
                    <input id="photoInput" type="file" name="photo" class="form-control">
                    <span id="photoError"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Sign in</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-3"></div>
</div>
</body>
</html>