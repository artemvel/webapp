<#include "macs.ftl">
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="/resources/js/customscripts.js"></script>
    <script type="text/javascript" src="/resources/js/validations.js"></script>

    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/custom_style.css" rel="stylesheet">

</head>
<body>
<div id="client_page_container" class="container-fluid">
    <div class="col-md-3">
        <div id="photo" class="col-md-12">
            <div class="thumbnail">
            <#if client.photo??>
                <img class="img-responsive" src="/client_img?id=${client.id}" alt="avatar">
            <#else>
                <img class="img-responsive" src="/resources/img/default.jpg">
            </#if>
                <div id="editInfo" class="caption">
                    <h3 id="editInfo">Info: </h3>
                <@client_info_table client/>
                    <input class="btn btn-info" onclick="getForm(${client.id})" style="width: 100%" id="edit"
                           type="button" value="Edit">
                </div>
            </div>
        </div>
        <div id="client_info"></div>
    </div>
    <div class="col-md-9">
        <h2>Credit Lines</h2>
        <ul class="nav nav-tabs">

        <#if client.creditLines?size != 0>
            <#list client.creditLines as creditLine>
                <#if creditLine?counter == 1>
                    <li class="active"><a data-toggle="tab" href="#${creditLine.id}">Line #${creditLine?counter}</a>
                    </li>
                <#else>
                    <li><a data-toggle="tab" href="#${creditLine.id}">Line #${creditLine?counter}</a></li>
                </#if>
            </#list>
            <li><a data-toggle="tab" href="#${client.creditLines?size + 1}">Add
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </a></li>
        <#else>
            <li class="active"><a data-toggle="tab" href="#${client.creditLines?size + 1}">Add
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </a></li>
        </#if>
        </ul>

        <div class="tab-content">
        <#list client.creditLines as creditLine>
            <#if creditLine?counter == 1>
                <div id="${creditLine.id}" class="tab-pane in active">
                    <div class="col-md-4"><@cl_table creditLine/></div>
                    <div class="col-md-8">
                        <div id="chart${creditLine.id}"></div>
                    </div>
                    <div class="col-md-12">
                        <@py_table creditLine/>
                    </div>
                </div>
            <#else>
                <div id="${creditLine.id}" class="tab-pane">
                    <div class="col-md-4"><@cl_table creditLine/></div>
                    <div class="col-md-8">
                        <div id="chart${creditLine.id}"></div>
                    </div>
                    <div class="col-md-12">
                        <@py_table creditLine/>
                    </div>
                </div>
            </#if>
        </#list>

        <#if client.creditLines?size != 0>
        <div id="${client.creditLines?size + 1}" class="tab-pane">
        <#else>
        <div id="${client.creditLines?size + 1}" class="tab-pane in active">
        </#if>

            <div class="panel panel-success">
                <div class="panel-heading">Add new credit line</div>
                <div class="panel-body">

                    <form id="creditLineForm" method="post" class="form-horizontal"
                          action="/createNewCreditLine?clientId=${client.id}">
                        <label class="col-md-3 control-label">Months: </label>
                        <div class="col-md-8 form-group">
                            <label class="radio-inline">
                                <input type="radio" value="12" checked="checked" name="months"> 12
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="24" name="months"> 24
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="36" name="months"> 36
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="48" name="months"> 48
                            </label>
                            <label class="radio-inline">
                                <input type="radio" value="60" name="months"> 60
                            </label>
                        </div>

                        <label class="col-md-3 control-label">Currency: </label>
                        <div class="col-md-8 form-group">
                            <input id="uah" class="btn btn-default btn-sm active" type="button" value="UAH">
                            <input id="usd" class="btn btn-default btn-sm" type="button" value="USD">
                            <input id="eur" class="btn btn-default btn-sm" type="button" value="EUR">
                        </div>

                        <label class="col-md-3 control-label">Amount: </label>
                        <div class="col-md-8 input-group">
                            <span id="currencyInput" class="input-group-addon">UAH</span>
                            <input id="creditLineLimit" type="text" name="limit" class="form-control"
                                   aria-label="Amount (to the nearest dollar)">
                            <span class="input-group-addon">.00</span>
                        </div>
                        <br>
                        <label class="col-md-3 control-label">SCHEMA: </label>
                        <div class="col-md-8 form-group">
                            <input id="classic" class="btn btn-default btn-sm active" type="button" value="CLASSIC">
                            <input id="annuity" class="btn btn-default btn-sm" type="button" value="ANNUITY">
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-6 col-sm-10">
                                <button type="submit" class="btn btn-default">Create</button>
                            </div>
                        </div>

                        <input id="currency" type="hidden" value="UAH" name="currency">
                        <input id="schema" type="hidden" value="CLASSIC" name="paymentSchema">

                    </form>
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            google.charts.load('current', {'packages': ['corechart']});

        <#list client.creditLines as creditLine>

        $.ajax({
            url: '/getChart?id=${creditLine.id}',
            success: function (result){
                google.charts.setOnLoadCallback(function () {
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Topping');
                    data.addColumn('number', 'Slices');
                    data.addRows([
                            ['Debt overall', result[1]],
                            ['Already paid', result[0]]
                    ]);
                    var options = {
                        'title': 'How Much to pay off debt',
                        'width': 400,
                        'height': 300
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('chart${creditLine.id}'));
                    chart.draw(data, options);
                });
            }
        });
        </#list>
        });
    </script>

</body>
</html>