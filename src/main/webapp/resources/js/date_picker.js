$(document).ready(function () {
    $("#dateOfBirthInput").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-90:+0",
        dateFormat: "yy-mm-dd"
    });
});
