var namesPattern = /^[A-z]+$/;
var emailPattern = /^\w+([.\w]+)*\w@\w((\.\w)*\w+)*\.\w{2,3}$/;
var digitPattern = /^[0-9]+$/;
var phonePattern = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;

var name = false;
var surname = false;
var patronymic = false;
var email = false;
var creditCard = false;
var tin = false;
var hAddress = false;
var birthDate = false;
var phone = false;

$(document).ready(function () {

    $("#nameInput").keyup(function () {
        if ($(this).val().length > 1) {
            if (namesPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#nameError').text('');
                name = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#nameError').text('Name should consist of letters only');
                name = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#nameError').text('Number of letters more than one and no empty');
            name = false;
        }
    });

    $("#surnameInput").keyup(function () {
        if ($(this).val().length > 1) {
            if (namesPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#surnameError').text('')
                surname = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#surnameError').text('Surname should consist of letters only');
                surname = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#surnameError').text('Number of letters more than one and no empty');
            surname = false;
        }
    });

    $("#patronymicInput").keyup(function () {
        if ($(this).val().length > 1) {
            if (namesPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#patronymicError').text('');
                patronymic = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#patronymicError').text('Patronymic name should consist of letters only');
                patronymic = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#patronymicError').text('Number of letters more than one and no empty');
            patronymic = false;
        }
    });

    $("#emailInput").keyup(function () {
        if ($(this).val() != '') {
            if (emailPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#emailError').text('');
                email = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#emailError').text('Incorrect password. Example: example@somesite.domain');
                email = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#emailError').text('Field cant be empty');
            email = false;
        }
    });

    $("#phoneInput").keyup(function () {
        if ($(this).val() != '') {
            if (phonePattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#phoneError').text('');
                phone = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#phoneError').text('Incorrect phone number. Example: 0991234565');
                phone = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#phoneError').text('Field cant be empty');
            phone = false;
        }
    });

    $("#creditCardNumberInput").keyup(function () {
        if ($(this).val().length == 16) {
            if (digitPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#creditCardNumberError').text('');
                creditCard = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#creditCardNumberError').text('Input should consist of only digits');
                creditCard = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#creditCardNumberError').text('Field cant be empty and consist of 16 digits');
            creditCard = false;
        }
    });

    $("#tinInput").keyup(function () {
        if ($(this).val().length == 12) {
            if (digitPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#tinError').text('');
                tin = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#tinError').text('Input should consist of only digits');
                tin = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#tinError').text('Field cant be empty and consist of 12 digits');
            tin = false;
        }
    });

    $("#homeAddressInput").keyup(function () {
        if ($(this).val().length >= 4) {
            $(this).css({'border': '1px solid #569b44'});
            $('#homeAddressError').text('');
            hAddress = true;
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#homeAddressError').text('Fill this form (min 4 letters)');
            hAddress = false;
        }
    });

    $("#dateOfBirthInput").click(function () {
        if ($(this).val().length == 10) {
            $(this).css({'border': '1px solid #569b44'});
            $('#dateOfBirthError').text('');
            birthDate = true;
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#dateOfBirthError').text('Enter the correct date, Ex: 1991-11-11');
            birthDate = false;
        }
    });

    $("#client_form").submit(function (e) {
        if (name && surname && patronymic && email && hAddress && tin && creditCard && phone && birthDate) {
            return true;
        }else {
            alert('Some fields are invalid, fill them correct');
            e.preventDefault();
            return false;
        }
    });

    var crLine = false;

    $("#creditLineLimit").keyup(function () {
        if ($(this).val().length > 3) {
            if (digitPattern.test($(this).val())) {
                $(this).css({'border': '1px solid #569b44'});
                $('#creditLineLimitError').text('');
                crLine = true;
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#creditLineLimitError').text('Input should consist of only digits');
                crLine = false;
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#creditLineLimitError').text('Amount should consist of minimum 4 digits');
            crLine = false;
        }
    });

    $("#creditLineForm").submit(function (e) {

        var months = $('input[name=months]:checked').val();
        var limit = $("#creditLineLimit").val();
        var currency = $("#currency").val();
        var schema = $("#schema").val();

        if (crLine) {
            return confirm(
                'Are you sure? \n' +
                'months: ' + months + '\n' +
                'limit: ' + limit + '\n' +
                'currency: ' + currency + '\n' +
                'schema: ' + schema
            );
        }else {
            alert('Please enter a valid data');
            e.preventDefault();
            return false;
        }
    });
});