$(document).ready(function () {
    $('#uah').click(function () {
        $("#uah").removeClass("btn btn-default btn-sm");
        $(this).addClass("btn btn-default btn-sm active");

        $("#usd").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");

        $("#eur").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");

        $('#currency').val($(this).val());

        $("#currencyInput").html($(this).val());
    });

    $('#usd').click(function () {
        $("#usd").removeClass("btn btn-default btn-sm");
        $(this).addClass("btn btn-default btn-sm active");

        $("#uah").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");

        $("#eur").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");

        $('#currency').val($(this).val());

        $("#currencyInput").html($(this).val());
    });

    $('#eur').click(function () {
        $("#eur").removeClass("btn btn-default btn-sm");
        $(this).addClass("btn btn-default btn-sm active");

        $("#uah").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");

        $("#usd").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");

        $('#currency').val($(this).val());

        $("#currencyInput").html($(this).val());
    });

    $('#classic').click(function () {
        $("#classic").removeClass("btn btn-default btn-sm");
        $(this).addClass("btn btn-default btn-sm active");

        $("#annuity").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");
        $('#schema').val($(this).val());
    });

    $('#annuity').click(function () {
        $("#annuity").removeClass("btn btn-default btn-sm");
        $(this).addClass("btn btn-default btn-sm active");

        $("#classic").removeClass("btn btn-default btn-sm active").addClass("btn btn-default btn-sm");
        $('#schema').val($(this).val());
    });
});

/*edit profile scripts*/

function getForm(clientId) {
    $('#edit').attr('type', 'hidden');
    $("#editInfo").find("table tr td").each(function (index, elem) {
        var inputName = paramName(index);
        var value = elem.innerHTML;

        $(elem).html('<div id="' + inputName + 'Input" class="col-sm-12"><input type="text" name="' + inputName + '" class="form-control" value="' + value + '"><span id="' + inputName + 'Error"></span></div>');
    });

    var tableBody = $('table').html();
    var result = makeForm(tableBody, clientId);

    $('#editInfo').html(result);
}

function paramName(index) {
    switch (index) {
        case 0:
            return 'name';
            break;
        case 1:
            return 'surname';
            break;
        case 2:
            return 'patronymic';
            break;
        case 3:
            return 'dateOfBirth';
            break;
        case 4:
            return 'homeAddress';
            break;
        case 5:
            return 'email';
            break;
        case 6:
            return 'TIN';
            break;
        case 7:
            return 'creditCardNumber';
            break;
        case 8:
            return 'phone';
            break;
    }
}

function makeForm(tableBody, clientId){
    return '<h3 id="editInfo">Edit Info: </h3>' +
        '<form id="client_form" class="form-horizontal" action="/editInfo?id='+ clientId +'" method="post">' +
        '<table class="table table-condensed">' + tableBody + '</table>' +
        '<input class="btn btn-success" style="width: 100%" id="client_form" type="submit" value="Save">' +
        '</form>';
}