package net.riversoft.test.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "payments")
public class Payment extends AbstractModel {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "credit_line")
    private CreditLine creditLine;

    @Column(nullable = false, precision = 20, scale = 2)
    private BigDecimal amount;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "pay_day", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date payDay;

    @Column(name = "paid", nullable = false, columnDefinition = "BIT")
    private boolean isPaid;

    public Payment(BigDecimal amount, Date payDay, CreditLine creditLine) {
        this.amount = amount;
        this.payDay = payDay;
        this.creditLine = creditLine;
    }

    public Payment() {
    }

    public CreditLine getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(CreditLine creditLine) {
        this.creditLine = creditLine;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getPayDay() {
        return payDay;
    }

    public void setPayDay(Date payDay) {
        this.payDay = payDay;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "amount=" + amount +
                ", payDay=" + payDay +
                ", isPaid=" + isPaid +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Payment payment = (Payment) o;

        return amount.equals(payment.amount) && payDay.equals(payment.payDay);

    }

    @Override
    public int hashCode() {
        int result = amount.hashCode();
        result = 31 * result + payDay.hashCode();
        return result;
    }
}
