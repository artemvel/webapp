package net.riversoft.test.task.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "clients")
public class Client extends AbstractModel {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private String patronymic;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "date_of_birth")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfBirth;

    @Column(unique = true, length = 12, nullable = false, name = "tin")
    private String TIN;

    @Column(unique = true)
    private String email;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(nullable = false, name = "home_address")
    private String homeAddress;

    @Column(nullable = false, length = 16, name = "credit_card_number")
    private String creditCardNumber;

    @Column(nullable = false, length = 12, name = "phone")
    private String phone;

    @OneToMany(mappedBy = "creditOwner",
            cascade = CascadeType.ALL, orphanRemoval = true,
            fetch = FetchType.LAZY)
    private List<CreditLine> creditLines = new ArrayList<>();

    public Client() {
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getTIN() {
        return TIN;
    }

    public void setTIN(String TIN) {
        this.TIN = TIN;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public List<CreditLine> getCreditLines() {
        return creditLines;
    }

    public void setCreditLines(List<CreditLine> creditLines) {
        this.creditLines = creditLines;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", TIN='" + TIN + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", creditCardNumber='" + creditCardNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return name.equals(client.name)
                && surname.equals(client.surname)
                && patronymic.equals(client.patronymic)
                && dateOfBirth.equals(client.dateOfBirth)
                && TIN.equals(client.TIN)
                && email.equals(client.email)
                && phone.equals(client.phone)
                && homeAddress.equals(client.homeAddress)
                && creditCardNumber.equals(client.creditCardNumber)
                && creditLines.equals(client.creditLines);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + patronymic.hashCode();
        result = 31 * result + dateOfBirth.hashCode();
        result = 31 * result + TIN.hashCode();
        result = 31 * result + phone.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + homeAddress.hashCode();
        result = 31 * result + creditCardNumber.hashCode();
        result = 31 * result + creditLines.hashCode();
        return result;
    }
}
