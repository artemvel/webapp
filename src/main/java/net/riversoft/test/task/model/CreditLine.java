package net.riversoft.test.task.model;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

@Entity
@Table(name = "credit_lines")
public class CreditLine extends AbstractModel {

    @ManyToOne
    @JoinColumn(name = "credit_owner_id")
    private Client creditOwner;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "open_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date openDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "close_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closeDate;

    @Enumerated(value = EnumType.STRING)
    private Currency currency;

    @Column(nullable = false, precision = 20, scale = 2, name = "credit_limit")
    private BigDecimal limit;

    @OneToMany(mappedBy = "creditLine",
            cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Payment> payments = new LinkedList<>();

    private Double rate;

    private Integer period;

    @Column(name = "payment_schema")
    @Enumerated(value = EnumType.STRING)
    private Schema paymentSchema;

    @Column(name = "payment_status")
    @Enumerated(value = EnumType.STRING)
    private Status status;

    public enum Currency {UAH, USD, EUR}

    public enum Status {ACTIVE, CLOSED}

    public enum Schema {CLASSIC, ANNUITY}

    public CreditLine() {
    }

    public Client getCreditOwner() {
        return creditOwner;
    }

    public void setCreditOwner(Client creditOwner) {
        this.creditOwner = creditOwner;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getLimit() {
        return limit;
    }

    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Double getRate() {
        return rate;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Schema getPaymentSchema() {
        return paymentSchema;
    }

    public void setPaymentSchema(Schema paymentSchema) {
        this.paymentSchema = paymentSchema;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CreditLine{" +
                "openDate=" + openDate +
                ", closeDate=" + closeDate +
                ", currency=" + currency +
                ", limit=" + limit +
                ", rate=" + rate +
                ", paymentSchema=" + paymentSchema +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditLine that = (CreditLine) o;
        return openDate.equals(that.openDate)
                && closeDate.equals(that.closeDate)
                && currency == that.currency
                && limit.equals(that.limit)
                && payments.equals(that.payments)
                && rate.equals(that.rate)
                && paymentSchema == that.paymentSchema
                && status == that.status;
    }

    @Override
    public int hashCode() {
        int result = openDate.hashCode();
        result = 31 * result + closeDate.hashCode();
        result = 31 * result + currency.hashCode();
        result = 31 * result + limit.hashCode();
        result = 31 * result + payments.hashCode();
        result = 31 * result + rate.hashCode();
        result = 31 * result + paymentSchema.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }
}
