package net.riversoft.test.task.service;

import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.dao.DaoImpls.CreditLineDao;
import net.riversoft.test.task.model.CreditLine;
import net.riversoft.test.task.model.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class CrLineService {

    private final static Logger logger = LoggerFactory.getLogger(CrLineService.class);

    private static final long DAYS_IN_MONTH = 31L;
    private static final long MILLS_IN_MONTH = DAYS_IN_MONTH * 24 * 60 * 60 * 1000L;
    private static final long DAYS_IN_YEAR = 365L;

    @Autowired
    private CreditLineDao creditLineDao;

    public CreditLine createNewCreditLine(CreditLine line) throws ServiceException {
        logger.info("createNewCreditLine(...) -> " + line);
        CreditLine creditLine;
        try {
            List<Payment> paymentsBySchema = calculateAndFill(line);
            line.setPayments(paymentsBySchema);
            creditLine = creditLineDao.create(line);
        } catch (DaoException e) {
            logger.warn("cant create line", e);
            throw new ServiceException();
        }
        logger.info("createNewCreditLine(...) -> return" + creditLine);
        return creditLine;
    }

    private List<Payment> calculateAndFill(CreditLine creditLine) {
        logger.info("calculateAndFill(...) -> " + creditLine);
        if (creditLine.getPaymentSchema() == CreditLine.Schema.ANNUITY) {
            return calculateForAnnuitySchema(creditLine);
        } else {
            return calculateForClassicSchema(creditLine);
        }
    }

    private List<Payment> calculateForClassicSchema(CreditLine creditLine) {
        logger.info("calculateForClassicSchema(...) -> " + creditLine);
        List<Payment> payments = new LinkedList<>();
        Payment payment;
        BigDecimal creditBody = creditLine.getLimit();
        BigDecimal bdRate = BigDecimal.valueOf(creditLine.getRate() / 100.00);

        Date date = new Date();
        for (int i = 0; i < creditLine.getPeriod(); i++) {
            BigDecimal percent = creditBody.multiply(bdRate).multiply(BigDecimal.valueOf(DAYS_IN_MONTH)).divide(BigDecimal.valueOf(DAYS_IN_YEAR), 2);
            BigDecimal body = creditLine.getLimit().divide(BigDecimal.valueOf(creditLine.getPeriod()), 2);
            Date currentMonthDate = new Date(date.getTime() + (i * MILLS_IN_MONTH));
            payment = new Payment(percent.add(body), currentMonthDate, creditLine);
            payment.setPaid(false);
            payments.add(payment);
            creditBody = creditBody.subtract(body);
        }
        return payments;
    }

    private List<Payment> calculateForAnnuitySchema(CreditLine creditLine) {
        logger.info("calculateForAnnuitySchema(...) -> " + creditLine);
        List<Payment> payments = new LinkedList<>();
        Payment payment;

        double k = (Math.pow(1 + creditLine.getRate() / 100.00, (double) 1 / 12)) - 1;

        logger.trace("coefficient per month " + k);
        double paymentPerMonth = (k * Math.pow((1 + k), creditLine.getPeriod())) / ((Math.pow((1 + k), creditLine.getPeriod())) - 1);

        logger.trace("paymentPerMonth " + paymentPerMonth);
        BigDecimal limit = creditLine.getLimit().multiply(BigDecimal.valueOf(paymentPerMonth));
        limit = limit.setScale(2, RoundingMode.CEILING);
        logger.trace("limit " + limit);

        Date date = new Date();
        for (int i = 0; i < creditLine.getPeriod(); i++) {
            payment = new Payment();
            Date currentMonthDate = new Date(date.getTime() + (i * MILLS_IN_MONTH));
            payment.setPayDay(currentMonthDate);
            payment.setCreditLine(creditLine);
            payment.setAmount(limit);
            payment.setPaid(false);
            payments.add(payment);
        }
        return payments;
    }

    public void updateCreditLine(CreditLine line) throws ServiceException {
        logger.info("updateCreditLine(...) -> " + line);
        try {
            List<Payment> payments = line.getPayments();
            boolean isClosed = true;
            for (Payment payment: payments){
                if (!payment.isPaid()){
                    isClosed = false;
                    break;
                }
            }
            logger.trace("line isClosed = " + isClosed);
            if (isClosed){
                line.setStatus(CreditLine.Status.CLOSED);
            }
            creditLineDao.update(line);
        } catch (DaoException e) {
            logger.warn("cant update line", e);
            throw new ServiceException();
        }
    }

    public CreditLine getCreditLineById(int id) throws ServiceException {
        logger.info("getCreditLineById(...) -> " + id);
        CreditLine creditLine;
        try {
            creditLine = creditLineDao.getById(id);
            creditLine.getPayments().size(); // INIT
        } catch (DaoException e) {
            logger.warn("cant retrieve credit line", e);
            throw new ServiceException();
        }
        logger.info("getCreditLineById(...) -> return" + creditLine);
        return creditLine;
    }

    public List<CreditLine> getAllCreditLines() throws ServiceException {
        logger.info("getAllCreditLines(...) -> ");
        List<CreditLine> creditLines;
        try {
            creditLines = creditLineDao.getAll();
        } catch (DaoException e) {
            logger.warn("cant retrieve credit lines", e);
            throw new ServiceException("Exception in DAO layer", e);
        }
        logger.info("getAllCreditLines(...) -> return" + creditLines);
        return creditLines;
    }

    public void deleteCreditLine(CreditLine line) throws ServiceException {
        logger.info("deleteCreditLine(...) -> " + line);
        try {
            creditLineDao.delete(line);
        } catch (DaoException e) {
            logger.warn("cant delete credit lines", e);
            throw new ServiceException();
        }
    }

}
