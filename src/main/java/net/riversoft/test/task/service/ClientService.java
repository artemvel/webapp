package net.riversoft.test.task.service;

import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.dao.DaoImpls.ClientDao;
import net.riversoft.test.task.model.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClientService {

    private final static Logger logger = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientDao clientDao;

    public Client createNewClient(Client client) throws ServiceException {
        logger.info("createNewClient(...) -> " + client);
        Client cl;
        try {
            cl = clientDao.create(client);
        } catch (DaoException e) {
            logger.warn("cant create client", e);
            throw new ServiceException();
        }
        logger.info("createNewClient(...) -> return" + cl);
        return cl;
    }

    public void updateClient(Client client) throws ServiceException {
        logger.info("updateClient(...) -> " + client);
        try {
            clientDao.update(client);
        } catch (DaoException e) {
            logger.warn("cant update client", e);
            throw new ServiceException();
        }
    }

    public Client getClientById(int id) throws ServiceException {
        logger.info("getClientById(...) -> " + id);
        Client client;
        try {
            client = clientDao.getById(id);
        } catch (DaoException e) {
            logger.warn("cant retrieve client", e);
            throw new ServiceException();
        }
        if (client != null) {
            client.getCreditLines().size();
        }
        logger.info("getClientById(...) -> return" + client);
        return client;
    }

    public List<Client> getAllClients() throws ServiceException {
        logger.info("getAllClients(...)");
        List<Client> clients;
        try {
            clients = clientDao.getAll();
        } catch (DaoException e) {
            logger.warn("cant retrieve list of client", e);
            throw new ServiceException();
        }
        logger.info("getAllClients(...) -> return" + clients);
        return clients;
    }
}
