package net.riversoft.test.task.dao.DaoImpls;

import net.riversoft.test.task.dao.AbstractDao;
import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.model.CreditLine;
import org.springframework.stereotype.Repository;


@Repository
public class CreditLineDao extends AbstractDao<CreditLine> {

    public CreditLineDao() {
        super(CreditLine.class);
    }

    public void delete(CreditLine creditLine) throws DaoException {
        em.remove(em.contains(creditLine) ? creditLine : em.merge(creditLine));
    }

    @Override
    public CreditLine getById(int id) throws DaoException {
        CreditLine creditLine = em.find(CreditLine.class, id);
        creditLine.getPayments().size(); // INIT
        return creditLine;
    }

}
