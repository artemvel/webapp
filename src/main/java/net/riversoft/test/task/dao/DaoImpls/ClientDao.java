package net.riversoft.test.task.dao.DaoImpls;

import net.riversoft.test.task.dao.AbstractDao;
import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.model.Client;
import org.springframework.stereotype.Repository;

@Repository
public class ClientDao extends AbstractDao<Client> {

    public ClientDao() {
        super(Client.class);
    }

    @Override
    public Client getById(int id) throws DaoException {
        Client client = em.find(Client.class, id);
        client.getCreditLines().size(); // INIT LAZY
        return client;
    }
}
