package net.riversoft.test.task.controller;

import net.riversoft.test.task.model.Client;
import net.riversoft.test.task.model.CreditLine;
import net.riversoft.test.task.model.Payment;
import net.riversoft.test.task.service.ClientService;
import net.riversoft.test.task.service.CrLineService;
import net.riversoft.test.task.service.ServiceException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Controller
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private CrLineService creditLineService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.setAllowedFields("name", "surname", "patronymic", "password",
                "email", "creditCardNumber", "homeAddress", "phone", "TIN", "dateOfBirth"
        );
    }

    @RequestMapping("/")
    public String startPage(Model model) throws ServiceException {
        List<Client> clients = clientService.getAllClients();
        model.addAttribute("clients", clients);
        return "index";
    }

    @RequestMapping(value = "/clientPage", method = RequestMethod.GET)
    public String clientPage(@RequestParam("id") int id, Model model) throws ServiceException {
        Client client = clientService.getClientById(id);
        model.addAttribute("client", client);
        return "client_page";
    }

    @RequestMapping(value = "/addClient", method = RequestMethod.GET)
    public ModelAndView addNewClient() {
        return new ModelAndView("new_client_form", "newClient", new Client());
    }

    @RequestMapping(value = "/createClient", method = RequestMethod.POST)
    public String registerNewClient(@ModelAttribute("newClient") Client client,
                                    @RequestParam(value = "photo", required = false) MultipartFile file, RedirectAttributes redirectAttributes) throws IOException, ServiceException {
        if (!file.isEmpty()) {
            client.setPhoto(file.getBytes());
        }
        Client createdClient = clientService.createNewClient(client);
        redirectAttributes.addAttribute("id", createdClient.getId());
        return "redirect:/clientPage?id={id}";
    }

    @ResponseBody
    @RequestMapping(value = "/client_img")
    public byte[] photoHandler(@RequestParam("id") int id) throws ServiceException {
        Client owner = clientService.getClientById(id);
        return owner.getPhoto();
    }

    private Date getCreditPeriod(int months) {
        long days = 0;
        switch (months) {
            case 12:
                days = 365;
                break;
            case 24:
                days = 730;
                break;
            case 36:
                days = 1095;
                break;
            case 48:
                days = 1460;
                break;
            case 60:
                days = 1825;
                break;
        }
        return new Date(new Date().getTime() + (days * 24 * 60 * 60 * 1000));
    }

    private double calculateRate(int months) {
        double result = 0;
        switch (months) {
            case 12:
                result = 22;
                break;
            case 24:
                result = 21;
                break;
            case 36:
                result = 20;
                break;
            case 48:
                result = 19;
                break;
            case 60:
                result = 18;
                break;
        }
        return result;
    }

    @RequestMapping("/editInfo")
    public String editClientInfo(@RequestParam("id") int clientId,
                                 @ModelAttribute("Client") Client client,
                                 RedirectAttributes redirectAttributes) throws ServiceException {

        Client oldInfoClient = clientService.getClientById(clientId);

        client.setId(clientId);
        client.setCreditLines(oldInfoClient.getCreditLines());
        client.setPhoto(oldInfoClient.getPhoto());

        clientService.updateClient(client);
        redirectAttributes.addAttribute("clientId", clientId);

        return "redirect:/clientPage?id={clientId}";
    }

    @RequestMapping("/createNewCreditLine")
    public String createNewCrLine(@RequestParam("clientId") int clientId,
                                  @RequestParam("paymentSchema") CreditLine.Schema schema,
                                  @RequestParam("currency") CreditLine.Currency currency,
                                  @RequestParam("limit") BigDecimal limit,
                                  @RequestParam("months") int months,
                                  RedirectAttributes redirectAttrs) throws ServiceException {

        CreditLine creditLine = new CreditLine();
        creditLine.setStatus(CreditLine.Status.ACTIVE);
        creditLine.setPaymentSchema(schema);
        creditLine.setCurrency(currency);
        BigDecimal fmt = limit.setScale(2, BigDecimal.ROUND_CEILING);
        creditLine.setLimit(fmt);
        creditLine.setOpenDate(new Date());
        creditLine.setCloseDate(getCreditPeriod(months));
        creditLine.setPeriod(months);
        creditLine.setRate(calculateRate(months));

        creditLine.setCreditOwner(clientService.getClientById(clientId));

        creditLineService.createNewCreditLine(creditLine);

        redirectAttrs.addAttribute("id", clientId);
        return "redirect:/clientPage?id={id}";
    }

    @ResponseBody
    @RequestMapping("/getChart")
    public BigDecimal[] getPayments(@RequestParam("id") int id) throws ServiceException {
        CreditLine creditLine = creditLineService.getCreditLineById(id);
        List<Payment> payments = creditLine.getPayments();

        BigDecimal overAllDebt = new BigDecimal(0);

        for (Payment payment : payments) {
            overAllDebt = overAllDebt.add(payment.getAmount());
        }

        CollectionUtils.filter(payments, new Predicate<Payment>() {
            @Override
            public boolean evaluate(Payment payment) {
                return payment.isPaid();
            }
        });

        BigDecimal paidAlready = new BigDecimal(0);
        for (Payment payment : payments) {
            BigDecimal amount = payment.getAmount();
            paidAlready = paidAlready.add(amount);
        }

        overAllDebt = overAllDebt.subtract(paidAlready);

        return new BigDecimal[]{paidAlready, overAllDebt};
    }
}
