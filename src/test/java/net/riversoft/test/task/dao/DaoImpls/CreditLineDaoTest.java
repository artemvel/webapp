package net.riversoft.test.task.dao.DaoImpls;

import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.model.Client;
import net.riversoft.test.task.model.CreditLine;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml", "classpath:context-override.xml"})
public class CreditLineDaoTest {

    @Autowired
    private CreditLineDao creditLineDao;

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private DataSource dataSource;

    private CreditLine creditLineTest;

    @Before
    public void setUp() throws FileNotFoundException, SQLException, ParseException, DaoException {
        RunScript.execute(dataSource.getConnection(),
                new FileReader("src/test/resources/create_insert_statements.sql"));

        creditLineTest = new CreditLine();
        creditLineTest.setCreditOwner(clientDao.getById(1));
        Date testDate = new Date(new GregorianCalendar(2016, 11, 17).getTimeInMillis());
        creditLineTest.setOpenDate(testDate);
        creditLineTest.setCloseDate(testDate);
        creditLineTest.setPeriod(1);
        creditLineTest.setCurrency(CreditLine.Currency.UAH);
        creditLineTest.setStatus(CreditLine.Status.ACTIVE);
        creditLineTest.setLimit(new BigDecimal(12000));
        creditLineTest.setRate(22.0);
        creditLineTest.setPaymentSchema(CreditLine.Schema.ANNUITY);
    }

    @Test
    @Transactional
    public void create() throws Exception {

        CreditLine creditLine = creditLineDao.create(creditLineTest);

        assertNotNull(creditLine);
        assertEquals(3, creditLine.getId().intValue());
    }

    @Test
    @Transactional
    public void getById() throws Exception {
        CreditLine actual = creditLineDao.getById(1);
        assertNotNull(actual);
        assertEquals(24, actual.getPeriod().intValue());
        assertEquals(CreditLine.Schema.CLASSIC, actual.getPaymentSchema());
    }

    @Test
    @Transactional
    public void update() throws Exception {
        CreditLine creditLine = creditLineDao.getById(1);
        creditLine.setPeriod(999);
        creditLineDao.update(creditLine);

        CreditLine actual = creditLineDao.getById(1);
        assertEquals(999, actual.getPeriod().intValue());
    }

    @Test
    @Transactional
    public void getAll() throws Exception {
        List<CreditLine> lines = creditLineDao.getAll();
        assertEquals(2, lines.size());

        creditLineDao.create(creditLineTest);
        lines = creditLineDao.getAll();
        assertEquals(3, lines.size());
    }
}