package net.riversoft.test.task.dao.DaoImpls;

import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.model.Client;
import net.riversoft.test.task.model.CreditLine;
import net.riversoft.test.task.model.Payment;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml", "classpath:context-override.xml"})
public class ClientDaoTest {

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private DataSource dataSource;

    private Client testClient;

    @Before
    public void setUp() throws FileNotFoundException, SQLException, ParseException {
        RunScript.execute(dataSource.getConnection(),
                new FileReader("src/test/resources/create_insert_statements.sql"));

        testClient = new Client();
        testClient.setName("Jonathan");
        testClient.setSurname("Presley");
        testClient.setPatronymic("Magamedov");
        testClient.setCreditCardNumber("1592634874860079");
        testClient.setEmail("maga@gmail.com");
        testClient.setHomeAddress("London, Shafsberry 14");
        testClient.setTIN("987654321058");
        testClient.setDateOfBirth(new GregorianCalendar(1990, 10, 11).getTime());
        testClient.setPhone("380957654456");

        CreditLine creditLine1 = new CreditLine();
        creditLine1.setCreditOwner(testClient);
        creditLine1.setRate(18.0);
        creditLine1.setOpenDate(new GregorianCalendar(2015, 11, 1).getTime());
        creditLine1.setCurrency(CreditLine.Currency.UAH);
        creditLine1.setLimit(new BigDecimal("50000.00"));
        creditLine1.setPaymentSchema(CreditLine.Schema.CLASSIC);
        creditLine1.setStatus(CreditLine.Status.ACTIVE);
        creditLine1.setCloseDate(new GregorianCalendar(2017, 11, 1).getTime());
        List<Payment> payments1 = new LinkedList<>();
        payments1.add(new Payment(new BigDecimal("1500.00"), new GregorianCalendar(2016, 1, 1).getTime(), creditLine1));
        payments1.add(new Payment(new BigDecimal("5500.00"), new GregorianCalendar(2016, 2, 1).getTime(), creditLine1));
        payments1.add(new Payment(new BigDecimal("4400.00"), new GregorianCalendar(2016, 3, 1).getTime(), creditLine1));
        creditLine1.setPayments(payments1);

        CreditLine creditLine2 = new CreditLine();
        creditLine2.setCreditOwner(testClient);
        creditLine2.setRate(19.0);
        creditLine2.setOpenDate(new GregorianCalendar(2015, 11, 1).getTime());
        creditLine2.setCurrency(CreditLine.Currency.UAH);
        creditLine2.setLimit(new BigDecimal("50000.00"));
        creditLine2.setPaymentSchema(CreditLine.Schema.CLASSIC);
        creditLine2.setStatus(CreditLine.Status.ACTIVE);
        creditLine2.setCloseDate(new GregorianCalendar(2019, 11, 1).getTime());
        List<Payment> payments2 = new LinkedList<>();
        payments2.add(new Payment(new BigDecimal("2500.00"), new GregorianCalendar(2016, 1, 1).getTime(), creditLine2));
        payments2.add(new Payment(new BigDecimal("3500.00"), new GregorianCalendar(2016, 2, 1).getTime(), creditLine2));
        creditLine2.setPayments(payments2);

        List<CreditLine> creditLines = new ArrayList<>();
        creditLines.add(creditLine1);
        creditLines.add(creditLine2);

        testClient.setCreditLines(creditLines);

    }

    @Test
    @Transactional
    public void getTEst() throws DaoException {
        Client client = clientDao.getById(1);

        assertEquals("John", client.getName());

        assertEquals(2, client.getCreditLines().size());
        assertEquals(3, client.getCreditLines().get(0).getPayments().size());
    }

    @Test
    @Transactional
    public void createTest() throws DaoException {
        Client actual = clientDao.create(testClient);
        Client expected = clientDao.getById(actual.getId());

        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getHomeAddress(), actual.getHomeAddress());

        assertEquals(2, expected.getCreditLines().size());
    }

    @Test
    @Transactional
    public void getAllTest() throws DaoException {
        List<Client> clients1 = clientDao.getAll();
        assertEquals(1, clients1.size());
        clientDao.create(testClient);

        List<Client> clients2 = clientDao.getAll();

        assertEquals(2, clients2.size());
    }

    @Test
    @Transactional
    public void updateTest() throws DaoException {
        testClient.setName("UpdatedName");
        testClient.getCreditLines().get(0).setPaymentSchema(CreditLine.Schema.ANNUITY);

        clientDao.update(testClient);

        Client client = clientDao.getById(2);

        assertEquals("UpdatedName", client.getName());
        assertEquals(CreditLine.Schema.ANNUITY, client.getCreditLines().get(0).getPaymentSchema());
    }
}