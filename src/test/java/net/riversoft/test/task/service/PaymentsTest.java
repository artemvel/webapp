package net.riversoft.test.task.service;

import net.riversoft.test.task.dao.DaoException;
import net.riversoft.test.task.dao.DaoImpls.ClientDao;
import net.riversoft.test.task.model.Client;
import net.riversoft.test.task.model.CreditLine;
import net.riversoft.test.task.model.Payment;
import org.h2.tools.RunScript;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.math.RoundingMode.CEILING;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml", "classpath:context-override.xml"})
public class PaymentsTest {

    @Autowired
    private CrLineService crLineService;

    @Autowired
    private ClientDao clientDao;

    @Autowired
    private DataSource dataSource;

    private CreditLine creditLine;

    @Before
    public void setUp() throws FileNotFoundException, SQLException, ParseException, DaoException {
        RunScript.execute(dataSource.getConnection(),
                new FileReader("src/test/resources/create_insert_statements.sql"));

        BigDecimal limit = new BigDecimal(10000);

        creditLine = new CreditLine();
        creditLine.setStatus(CreditLine.Status.ACTIVE);
        creditLine.setLimit(limit);
        creditLine.setCreditOwner(new Client());
        creditLine.setCurrency(CreditLine.Currency.UAH);
        creditLine.setOpenDate(new Date());
        creditLine.setCloseDate(new Date());
        creditLine.setCreditOwner(clientDao.getById(1));
    }

    @Test
    public void annuitySchemaCalculationCasesTest() throws ServiceException {
        creditLine.setPeriod(12);
        creditLine.setRate(22.0);

        creditLine.setPaymentSchema(CreditLine.Schema.ANNUITY);
        CreditLine cr = crLineService.createNewCreditLine(creditLine);
        List<Payment> payments = cr.getPayments();

        double[] actual = new double[]{
                926.59, 926.59, 926.59, 926.59,
                926.59, 926.59, 926.59, 926.59,
                926.59, 926.59, 926.59, 926.59
        };

        double[] expected = new double[creditLine.getPeriod()];

        for (int i = 0; i < payments.size(); i++) {
            expected[i] = payments.get(i).getAmount().doubleValue();
        }
        assertTrue(Arrays.equals(expected, actual));
    }

    @Test
    public void classicSchemaCalculationCasesTest() throws ServiceException {
        creditLine.setPeriod(12);
        creditLine.setRate(22.0);

        creditLine.setPaymentSchema(CreditLine.Schema.CLASSIC);
        CreditLine cr = crLineService.createNewCreditLine(creditLine);
        List<Payment> payments = cr.getPayments();

        double[] actual = new double[]{
                1020.85, 1005.27, 989.69, 974.10,
                958.52, 942.94, 927.35, 911.77,
                896.19, 880.61, 865.02, 849.44
        };

        double[] expected = new double[creditLine.getPeriod()];

        for (int i = 0; i < payments.size(); i++) {
            expected[i] = payments.get(i).getAmount().doubleValue();
        }
        assertTrue(Arrays.equals(expected, actual));
    }
}
