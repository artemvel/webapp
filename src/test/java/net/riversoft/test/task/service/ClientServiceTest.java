package net.riversoft.test.task.service;

import net.riversoft.test.task.dao.DaoImpls.ClientDao;
import net.riversoft.test.task.model.Client;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {

    @InjectMocks
    private ClientService clientService;

    @Mock
    private ClientDao clientDao;

    @Test
    public void createNewClient() throws Exception {
        Client client = new Client();

        clientService.createNewClient(client);

        verify(clientDao, times(1)).create(client);
    }

    @Test
    public void update() throws Exception {
        Client client = new Client();

        clientService.updateClient(client);

        verify(clientDao, times(1)).update(client);
    }

    @Test
    public void getClientById() throws Exception {
        Client client = new Client();

        when(clientDao.getById(1)).thenReturn(client);

        clientService.getClientById(1);

        verify(clientDao, times(1)).getById(1);
    }

    @Test
    public void getAllClients() throws Exception {
        List<Client> clients = new ArrayList<>();

        when(clientDao.getAll()).thenReturn(clients);

        clientService.getAllClients();

        verify(clientDao, times(1)).getAll();
    }
}
