package net.riversoft.test.task.service;

import net.riversoft.test.task.dao.DaoImpls.CreditLineDao;
import net.riversoft.test.task.model.Client;
import net.riversoft.test.task.model.CreditLine;
import net.riversoft.test.task.model.Payment;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CrLineServiceTest {


    @InjectMocks
    private CrLineService crLineService;

    @Mock
    private CreditLineDao creditLineDao;


    @Test
    public void updateCreditLine() throws Exception {
        CreditLine creditLine = new CreditLine();

        crLineService.updateCreditLine(creditLine);

        verify(creditLineDao, times(1)).update(creditLine);
    }

    @Test
    public void getCreditLineById() throws Exception {
        CreditLine creditLine = new CreditLine();
        when(creditLineDao.getById(1)).thenReturn(creditLine);

        crLineService.getCreditLineById(1);

        verify(creditLineDao, times(1)).getById(1);
    }

    @Test
    public void getAllCreditLines() throws Exception {
        List<CreditLine> clients = new ArrayList<>();

        when(creditLineDao.getAll()).thenReturn(clients);

        crLineService.getAllCreditLines();

        verify(creditLineDao, times(1)).getAll();
    }

    @Test
    public void deleteCreditLine() throws Exception {
        CreditLine creditLine = new CreditLine();

        crLineService.deleteCreditLine(creditLine);

        verify(creditLineDao, times(1)).delete(creditLine);
    }

}