DROP TABLE IF EXISTS clients;
DROP TABLE IF EXISTS credit_lines;
DROP TABLE IF EXISTS payments;

CREATE TABLE clients (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `patronymic` varchar(45) NOT NULL,
  `date_of_birth` date NOT NULL,
  `tin` VARCHAR(12) NOT NULL,
  `photo` LONGBLOB,
  `email` varchar(45) DEFAULT NULL,
  `home_address` varchar(45) NOT NULL,
  `credit_card_number` VARCHAR(16) NOT NULL,
  `phone` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `tin_UNIQUE` (`tin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE credit_lines (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `open_date` date NOT NULL,
  `close_date` date NOT NULL,
  `currency` varchar(3) NOT NULL,
  `credit_limit` decimal(20,2) NOT NULL,
  `rate` double NOT NULL,
  `period` INT(11),
  `payment_schema` varchar(7) NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `credit_owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`credit_owner_id`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE payments (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(20,2) NOT NULL,
  `pay_day` date NOT NULL,
  `credit_line` int(11) NOT NULL,
  `paid` BOOLEAN NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`credit_line`) REFERENCES `credit_lines` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO clients (id, name, surname, patronymic, credit_card_number, email, home_address, tin, date_of_birth, phone)
VALUES (1, 'John', 'Terry', 'Aristarhovich', '1234123412341234', 'jt@gmail.com',
        'London, baskevilley str., 8', '123456789012', '1991-11-17', '380947605603');

INSERT INTO credit_lines (id, credit_owner_id, rate, open_date, currency, credit_limit, payment_schema, payment_status, close_date, period)
VALUES (1, 1, 18.0, '2015-11-1', 'UAH', 50000.00, 'CLASSIC', 'ACTIVE', '2017-11-1', 24);

INSERT INTO payments (id, amount, pay_day, credit_line, paid)
VALUES (1, 2500.00, '2016-1-1', 1, TRUE),
  (2, 3500.00, '2016-2-1', 1, TRUE),
  (3, 1400.00, '2016-3-1', 1, FALSE);


INSERT INTO credit_lines (id, credit_owner_id, rate, open_date, currency, credit_limit, payment_schema, payment_status, close_date, period)
VALUES (2, 1, 18.0, '2015-11-1', 'UAH', 50000.00, 'CLASSIC', 'ACTIVE', '2019-11-1', 12);

INSERT INTO payments (id, amount, pay_day, credit_line, paid)
VALUES (4, 2500.00, '2016-1-1', 2, TRUE),
  (5, 3500.00, '2016-2-1', 2, TRUE),
  (6, 1400.00, '2016-3-1', 2, FALSE);