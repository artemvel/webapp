** Tools: **
JDK 7, Spring 4, JPA 2 / Hibernate 4, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, FreeMarker, Tomcat 7, MySQL/H2 IntelliJIDEA.

STARTING PAGE  

![start page](/screenshots/start_page.png)  

SEARCHING  

![start page search](/screenshots/start_page_search.gif)  

CREATING NEW CLIENT VALIDATION  

![validation](/screenshots/validation.png)  

CLIENT PAGE WITH NO PAYMENTS  

![client page np](/screenshots/client_page.png)  

CHANGING INFO  

![modify](/screenshots/js_read-write_option.gif)  

PAGE WITH PAYMENTS  

![page with p](/screenshots/client_page_with_payments.png)  

CREATE NEW CREDIT LINE  

![new credit line](/screenshots/create_new_creditline.gif)  

DEMO CHART WITH PAYMENTS MADE  

![payments chart demo](/screenshots/chart_demo.png)  

CREDIT LINE WITH CLASSIC payment schema  

![CLASSIC](/screenshots/classic_schema.png)  

CREDIT LINE WITH ANNUITY payment schema  

![ANNUITY](/screenshots/annuity_schema.png)  
  
MORE SCREEN  

![more](/screenshots/more.gif)  

**VELYKORODNYI ARTEM**  
RIVER_SOFT_TEST_TASK